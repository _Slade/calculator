This is a custom-built circuit that mimics the behavior of a dollar store calculator. The file `calculatorCPU.circ` is a [Logisim](http://www.cburch.com/logisim/) circuit file for a custom circuit containing a CPU, a register file, input buttons, and a screen. This circuit understands a subset of MIPS assembly language consisting of the following instructions:

* add, addi, and sub
* and, andi, or, ori, and nor
* sll and srl
* slt, slti, beq, and bne
* j

The file `calculator.mips` contains the assembly code ([MARS](http://courses.missouristate.edu/KenVollmar/MARS/) format, with m4 as a preprocessor) that powers the device. This assembly is used to generate the `firmware` file, which may be loaded into the circuit in Logisim.