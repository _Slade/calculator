divert(-1)
# Reserved registers
define(`OP1', $s7)
define(`OP2', $s6)
define(`OPR8R', $s5)
define(`RES', $s4)
define(`IN', $t9)
define(`INCP', $t0)
define(`DISP', $t8)
# Constants
define(`EQUAL', 14)
define(`CLEAR', 15)
#############################################################################
divert(0)dnl
state_0:
    add     DISP, $zero, $zero
    add     OPR8R, $zero, $zero
    add     OP1, $zero, $zero
    add     OP2, $zero, $zero
################################################################################
state_1:
    add     IN, $zero, $zero     # Ready for input
s1_wait: beq $t9, $zero, s1_wait # Wait for input
    add     $t0, $zero, $t9      # Copy input into INCP
    sll     $t0, $t0, 1          # input_copy <<=  1
    srl     $t0, $t0, 1          # input_copy >>>= 1

    addi    $t1, $zero, EQUAL
beq INCP, $t1, s1_eq             # Check if input was = or C
    addi    $t1, $zero, CLEAR
beq INCP, CLEAR, state_0         # "

    add     $t1, $zero, $zero    # Clear $t1-$t3
    add     $t2, $zero, $zero    # "
    add     $t3, $zero, $zero    # "
    slti    $t1, INCP, 10        # $t1 = $t0 < 10
    addi    $t2, $zero, -1       # $t2 = -1
    slt     $t3, $t2, INCP       # $t3 = -1 < $t0
    and     $t1, $t1, $t3        # $t1 = $t1 & $t3

beq $t1, $zero, s1_op            # if (0 <= $t1 <= 9)
    add     $t1, OP1, $zero      # Copy operand1
                                 # $s7 = (operand1 * 10) + input
    sll     OP1, OP1, 3          # operand1 << 3 == operand1 * 8
    add     OP1, OP1, $t1
    add     OP1, OP1, $t1
    add     OP1, OP1, INCP       # operand1 += input digit
    add     DISP, $zero, OP1     # Display operand1
j   state_1                      # Go to state 1

s1_op:                           # Input was an operator
    add     OPR8R, $zero, INCP   # operator = input_copy
    add     DISP, $zero, OP1     # Display OP1
    add     IN, $zero, $zero     # Ready for input
j   state_2                      # Go to state 2

s1_eq:                           # Input was "="
    add     RES, $zero, OP1      # result = operand1
    add     DISP, $zero, RES     # Display result
j   state_4
################################################################################
state_2:
    add  IN, $zero, $zero       # Ready for input
s2_wait: beq IN, $zero, s2_wait # Wait for input
    add     INCP, $zero, IN     # Copy input into input_copy
    sll     INCP, INCP, 1       # input_copy <<= 1
    srl     INCP, INCP, 1       # input_copy >>>= 1

    addi    $t1, $zero, EQUAL
beq INCP, $t1, s2_eq          # go to eq if input_copy == equal
    addi    $t1, $zero, CLEAR
beq INCP, $t1, state_0        # Reset if button was clear
    add     $t1, $zero, $zero   # Clear $t1-$t3
    add     $t2, $zero, $zero   # "
    add     $t3, $zero, $zero   # "
    slti    $t1, $t0, 10        # $t1 = $t0 < 10
    addi    $t2, $zero, -1      # $t2 = -1
    slt     $t3, $t2, $t0       # $t3 = -1 < $t0
    and     $t1, $t1, $t3       # $t1 = $t1 & $t3
beq $t1, $zero, s2_op
    add     $t1, OP2, $zero     # Copy operand2
                                # $s6 = (OP1 * 10) + input
    sll     OP2, OP2, 3         # operand2 << 3 == operand1 * 8
    add     OP2, OP2, $t1
    add     OP2, OP2, $t1
    add     OP2, OP2, INCP
    add     DISP, $zero, OP2
j state_3
s2_op:                          # Operator
    add     OPR8R, $zero, INCP  # operator = input
    add     DISP, $zero, OP1    # Display operand1
j state_2

s2_eq:                          # Equals sign
    add     RES, $zero, OP1     # result = operand1
    add     DISP, $zero, RES    # Display result
j state_4
################################################################################
state_3:
    add     IN, $zero, $zero    # Ready for input
s3_wait: beq IN, $zero, s3_wait # Loop until input
    add     INCP, $zero, IN     # move $t0, $t9
    sll     INCP, INCP, 1       # Clear MSB
    srl     INCP, INCP, 1

beq INCP, CLEAR, state_0

    add     $t1, $zero, $zero
    add     $t2, $zero, $zero
    add     $t3, $zero, $zero
    slti    $t1, INCP, 10       # $t1 = $t0 < 10
    addi    $t2, $zero, -1      # $t2 = -1
    slt     $t3, $t2, INCP      # $t3 = -1 < $t0
    and     $t1, $t1, $t3       # $t1 = $t1 & $t3

beq $t1, $zero, s3_op
    add     $t1, OP2, $zero     # Copy operand2
                                # $s7 = (operand2 * 10) + input
    sll     OP2, OP2, 3         # operand2 << 3 == operand2 * 8
    add     OP2, OP2, $t1
    add     OP2, OP2, $t1
    add     OP2, OP2, INCP      # operand2 += input digit
    add     DISP, $zero, OP2    # Display operand2
j   state_3

s3_op:                          # Operator
    addi    $t0, $zero, 10
beq OPR8R, $t0, addition
    addi    $t0, $zero, 11
beq OPR8R, $t0, subtract

# This is common to multiply and divide
    # $t2 will be true iff OP1 * OP2 is negative
    add     $t2, $zero, $zero
    slti    $t1, OP1, 0     # $t1 = op1 < 0
    xor     $t2, $t1, $t2   # $t2 = $t1 ^ $t1
    slti    $t1, OP2, 0
    xor     $t2, $t1, $t2   # $t2 = (OP1 < 0) ^ (OP2 < 0)

    # Set OP1 and OP2 to their absolute values
    sra     $t1, OP1, 31    # abs(OP1)
    xor     OP1, OP1, $t1   # "
    sub     OP1, OP1, $t1   # "

    sra     $t1, OP2, 31    # abs(OP2)
    xor     OP2, OP2, $t1   # "
    sub     OP2, OP2, $t1   # "

    addi    $t5, $zero, 12
beq OPR8R, $t5, multiply
    addi    $t5, $zero, 13
beq OPR8R, $t5, division

addition:
    add  RES, OP1, OP2
j   end_op

subtract:
    subu RES, OP1, OP2
j   end_op

multiply:
    # $t1 General purpose temp
    # $t2 is_negative
    # $t3 operand1 initial value

    # Optimization: ensure OP2 is the smaller of the two operands
slt     $t1, OP1, OP2 # OP1 < OP2
beq     $t1, $zero, minmaxelse
    add     $t1, $zero, OP2 # swap(OP1, OP2)
    add     OP2, $zero, OP1
    add     OP1, $zero, $t1
    minmaxelse:

    add     $t3, $zero, OP1
    add     OP1, $zero, $zero
mult_while:  # 0 < OP2
slt     $t1, $zero, OP2
beq     $t1, $zero, mult_endwhile
    add     OP1, OP1, $t3
    addi    OP2, OP2, -1    # OP2 -= 1
j   mult_while
mult_endwhile:

    # Restore sign
beq $t2, $zero, not_negative
    subu OP1, $zero, OP1
not_negative:
    add     RES, $zero, OP1     # Display result
j   end_op

division:
beq     OP2, $zero, state_0     # Bail out if dividing by 0
slt     $t1, OP1, OP2
bne     $t1, $zero, state_0     # Result is 0 if OP1 < OP2

    add     $t3, $zero, $zero   # int $t3 = 0;
div_while:                      # while (a >= b) {
    sub     OP1, OP1, OP2       #     OP1 -= OP2;
    addi    $t3, $t3, 1         #     $t3 += 1;
slt     $t1, OP1, OP2
beq     $t1, $zero, div_while

    # Restore sign
beq $t2, $zero, not_negative2
    sub     $t3, $zero, $t3
not_negative2:
    add     RES, $zero, $t3     # Display result
#j  end_op  (fallthrough)

end_op:                     # If we got here because of an equals press
    add  DISP, $zero, RES   # First display the result
    add  OP2, $zero, $zero
    addi    $t5, $zero, EQUAL
beq INCP, $t5, state_4    # then go to state 4
    add  OP1, $zero, RES    # Else, operand1 = result
    add  OPR8R, $zero, INCP # operator = input
j   state_2                 # go to state 2
################################################################################
state_4:
    add     IN, $zero, $zero # Ready for input
s4_wait: beq     IN, $zero, s4_wait

    add     INCP, $zero, IN
    sll     INCP, INCP, 1
    srl     INCP, INCP, 1

    addi    $t5, $zero, EQUAL
beq INCP, $t5, s4_eq
    addi    $t5, $zero, CLEAR
beq INCP, $t5, state_0

    add     $t1, $zero, $zero # Clear $t1-$t3
    add     $t2, $zero, $zero # "
    add     $t3, $zero, $zero # "
    slti    $t1, INCP, 10     # $t1 = $t0 < 10
    addi    $t2, $zero, -1    # $t2 = -1
    slt     $t3, $t2, INCP    # $t3 = -1 < $t0
    and     $t1, $t1, $t3     # $t1 = $t1 & $t3

beq $t1, $zero, s4_op
    add     OP1, $zero, INCP  # INCP =~ [0-9]
    add     DISP, $zero, OP1
j   state_1
s4_op:                   # Operator
    add     OP1, $zero, RES
    add     OPR8R, $zero, INCP
j   state_2

s4_eq:
    add     DISP, $zero, RES
j   state_4

