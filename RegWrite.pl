#!/usr/bin/perl
use 5.020;
use warnings;
use strict;
use List::AllUtils qw(uniq);

chomp(my @lines = <DATA>);
my @disjunctions;
for my $alu_bit (0 .. 3)
{
    for my $line ($lines)
    {
        my @fields = split $line;
        my $ALUOp  = pop @fields;
        for my $bits (@fields)
        {
            my @conjunctions;
            for (0 .. $#bits)
            {
                my $bit = $bits[$_];
                push @conjunctions, sprintf(
                    "%sx%d",
                    $bit == 0 ? '~' : '',
                    $#bits - $_
                );
            }
            push @disjunctions, '(' . join(' & ', @conjunctions) . ')';
        }
    }
    @disjunctions = uniq @disjunctions;
    print join ' + ', @disjunctions;
}
__DATA__
000000  100000  0010
001000  XXXXXX  0010
000000  100100  0000
001100  XXXXXX  0000
000000  100010  0110
000000  100101  0001
001101  XXXXXX  0001
000000  100111  1100
000000  101010  0111
001010  XXXXXX  0111
000100  XXXXXX  0110
000101  XXXXXX  0110
